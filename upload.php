<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Partition Museum Project</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="film-clips-page">
      <header class="header">
        <div class="back-icon">
          <a href="index.php"><i class="fa fa-angle-left"></i></a>
        </div>
        <div class="page-title">
          <h1>Upload Video</h1>
        </div>
      </header>
      <div class="film-clips photos">
        <div class="single-item mb20">
          <div class="play-icon">
            <i class="fa fa-play"></i>
          </div>
        </div>
      </div>
      <div class="upload-video">
        <form>
          <div class="form-group username">
            <label for="title">Title</label>
            <input type="text" name="title" required>
          </div>
          <div class="form-group emailid">
            <label for="description">Description</label>
            <input type="text" name="description" required>
          </div>
          <div class="form-group pannumber">
            <label for="name">Name</label>
            <input type="text" name="name" required>
          </div>
          <div class="form-group phonenumber">
            <label for="phone">Phone number</label>
            <input type="tel" pattern="[789][0-9]{9}" name="phone" required>
          </div>
          <div class="form-group">
            <input type="button" name="submit" value="SUBMIT FOR REVIEW">
          </div>
        </form>
      </div>
    </div>
    <?php include('footer.php'); ?>
