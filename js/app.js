$(document).ready(function(){
  // navigation slide
  // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  //   centerLI(e.target, 'ul.nav.nav-tabs');
  // //  console.log('e.target');
  // });
  $('body').on('click', 'a[data-toggle="tab"]', function(){
    centerLI(this, 'ul.nav.nav-tabs');
  });
  function centerLI(target, outer) {
    var out = $(outer);
    var tar = $(target);
    var x = out.width() - 50;
    var y = tar.outerWidth(true);
    var z = tar.index();
    var q = 0;
    var m = out.find('li');
    for (var i = 0; i < z; i++) {
      q += $(m[i]).outerWidth(true);
    }
    //out.scrollLeft(Math.max(0, q - (x - y)/2));
    out.animate({
      scrollRight: Math.max(0, q - (x - y) / 2)
    }, 500);
  }

  //Change Icon on Accordion
  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".glyphicon-one-fine-dot").addClass("small-circle").removeClass("glyphicon-one-fine-dot");
    }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".small-circle").addClass("glyphicon-one-fine-dot").removeClass("small-circle");
  });

  // //Change Icon on Accordion
  // $('.collapse').on('shown.bs.collapse', function(){
  //   $(this).parent().find(".small-circle").addClass("small-circle").removeClass("glyphicon-one-fine-dot");
  //   }).on('hidden.bs.collapse', function(){
  //   $(this).parent().find(".glyphicon-one-fine-dot").addClass("glyphicon-one-fine-dot").removeClass("small-circle");
  // });

  // validate form
  function validateName(){
    if($('.username input').val().length < 3 || $('.username input').val() == '')
    {
      $('.username label').css('color','#e80a0a');
      $('.username input').css('border-color','#e80a0a');
      $('.username').addClass('shake');
    }
    else
    {
      $('.username label').css('color','#a09f9f');
      $('.username input').css('border-color','#a09f9f');
    }
    return $('.username input').val();
  }
  function validateEmail() {
    var email = $('.emailid input').val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email) || email == '') {
      $('.emailid label').css('color','#e80a0a');
      $('.emailid input').css('border-color','#e80a0a');
      $('.emailid').addClass('shake');
    }
    else
    {
      $('.emailid label').css('color','#a09f9f');
      $('.emailid input').css('border-color','#a09f9f');
    }
    return $('.emailid input').val();
  }
  function validatePhonenumber(){
    if($('.phonenumber input').val().length != 10)
    {
      $('.phonenumber label').css('color','#e80a0a');
      $('.phonenumber input').css('border-color','#e80a0a');
      $('.phonenumber').addClass('shake');
    }
    else
    {
      $('.phonenumber label').css('color','#a09f9f');
      $('.phonenumber input').css('border-color','#a09f9f');
    }
    return $('.phonenumber input').val();
  }
  function validateAmount(){
    if($('.amount input').val().length < 1)
    {
      $('.amount label').css('color','#e80a0a');
      $('.amount input').css('border-color','#e80a0a');
      $('.amount').addClass('shake');
    }
    else
    {
      $('.phonenumber label').css('color','#a09f9f');
      $('.phonenumber input').css('border-color','#a09f9f');
    }
    return $('.phonenumber input').val();
  }

  // username
  $(".username input").focus(function(){
    $('.username label').addClass('top');
  });
  $(".username input").blur(function(){
    if($(this).val().length == 0){
      $(this).closest('.username').find('label').removeClass('top');
    }
    else {
      validateName();
    }
  });
  //emailid
  $(".emailid input").focus(function(){
    $('.emailid label').addClass('top');
  });
  $(".emailid input").blur(function(){
    if($(this).val().length == 0){
      $('.emailid label').removeClass('top');
    }
    else {
      validateEmail();
    }
  });
  // phonenumber
  $(".phonenumber input").focus(function(){
    $('.phonenumber label').addClass('top');
  });
  $(".phonenumber input").blur(function(){
    if($(this).val().length == 0){
      $('.phonenumber label').removeClass('top');
    }
    else {
      validatePhonenumber();
    }
  });
  // amount
  $(".amount input").focus(function(){
    $('.amount label').addClass('top');
  });
  $(".amount input").blur(function(){
    if($(this).val().length == 0){
      $('.amount label').removeClass('top');
    }
    else {
      validateAmount();
    }
  });
  // pannumber
  $(".pannumber input").focus(function(){
    $('.pannumber label').addClass('top');
  });
  $(".pannumber input").blur(function(){
    if($(this).val().length == 0){
      $('.pannumber label').removeClass('top');
    }
  });
  // message
  $(".message input").focus(function(){
    $('.message label').addClass('top');
  });
  $(".message input").blur(function(){
    if($(this).val().length == 0){
      $('.message label').removeClass('top');
    }
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    // hide video upload on on join/donate
    var x = e.target;
    if($(x).attr('href') == '#donate'){
      $('.fixed-video-icon').hide();
    }
    else {
      $('.fixed-video-icon').show();
    }
  });
  $('a.close').click(function(e){
  //  e.defaultPrevent();
    window.close();
  });
});
