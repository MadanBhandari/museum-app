<?php include('header.php'); ?>
<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#timeline" aria-controls="timeline" role="tab" data-toggle="tab">timeline</a></li>
    <li role="presentation"><a href="#oral-history" aria-controls="oral-history" role="tab" data-toggle="tab">oral history</a></li>
    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">events</a></li>
    <li role="presentation"><a href="#about-us" aria-controls="about-us" role="tab" data-toggle="tab">about us</a></li>
    <li role="presentation"><a href="#literature" aria-controls="literature" role="tab" data-toggle="tab">literature</a></li>
    <li role="presentation"><a href="#team" aria-controls="team" role="tab" data-toggle="tab">team</a></li>
    <li role="presentation"><a href="#shop" aria-controls="shop" role="tab" data-toggle="tab">shop</a></li>
    <li role="presentation"><a href="#donate" aria-controls="donate" role="tab" data-toggle="tab">join/donate</a></li>
    <li role="presentation"><a href="#map-img" aria-controls="map" role="tab" data-toggle="tab">map</a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="landing-page">
        <div class="main-title">
          <h1>THE PARTITION MUSEUM</h1>
        </div>
        <div class="breif">
          <p>The main focus of The Arts and Cultural Heritage Trust is to set up a Partition Museum</p>
          <div class="dark-line"></div>
        </div>
        <div class="oral-history">
          <div class="section-title">
            ORAL HISTORY <span class="see-all-link"><a href="#">SEE ALL</a></span>
          </div>
          <div class="history-container">
            <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
            <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="visit-museum">
          <h2>VISIT THE MUSEUM</h2>
          <div class="dark-line"></div>
          <table>
            <tr>
              <td>Visiting timings:</td>
              <td class="strong">11am - 6pm</td>
            </tr>
            <tr>
              <td>Visiting days:</td>
              <td class="strong">Monday - Friday</td>
            </tr>
            <tr>
              <td>Ticket Price:</td>
              <td class="strong">&#8377;10 - &#8377;100</td>
            </tr>
          </table>
        </div>
        <div id="map"></div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="timeline">
      <div class="timeline">
        <div class="line"></div>
        <div class="panel-group" id="timeline-items" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="1935">
              <div class="icon"> <i class="glyphicon small-circle"></i></div>
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#timeline-items" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  1935
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="1935">
              <div class="panel-body">
                <p>Anim pariatur cliche reprehenderit, heard of them accusamus labore sustainable VHS.</p>
                <p><img src="public/img/landing-title.png" alt="" /></p>
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <div class="icon"> <i class="glyphicon glyphicon-one-fine-dot"></i></div>
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#timeline-items" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  1937
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <div class="icon"> <i class="glyphicon glyphicon-one-fine-dot"></i></div>
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#timeline-items" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  1942
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="oral-history">
      <div class="oral-history">
        <P>Do you come from a Partition family? Share your families story for the Museum archive so that your family history is preserved for the future. Join our Grandparents/Grandkids movement to record our family stories!</P>
        <P>It’ll help you start a conversation and learn more about your family than you may yet know! Upload your video <a href="#">here</a> or email us with subject ‘Grandparents/Grandkids’ if you want some advice on how to kick-start the conversation!</P>
        <div class="history-container">
          <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
          <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
          <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="events">
      <div class="events-tab">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#upcomming-events" aria-controls="timeline" role="tab" data-toggle="tab">Upcomming Events</a></li>
          <li role="presentation"><a href="#past-events" aria-controls="timeline" role="tab" data-toggle="tab">Past Events</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="upcomming-events">
            <div class="landing-page">
              <div class="main-title">
                <h1>THE PARTITION MUSEUM</h1>
              </div>
              <div class="single-event">
                <h2>UPCOMMING EVENTS</h2>
                <div class="dark-line"></div>
                <div class="breif">
                  We are very excited to announce the launch of the Museum on November 1, 2016! Do join us at the Town Hall in Amritsar for a historic inauguration of the world’s first museum on the Partition of India.
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="past-events">
            <div class="landing-page">
              <div class="main-title">
                <h1>THE PARTITION MUSEUM</h1>
              </div>
              <div class="single-event">
                <h2>PAST EVENTS</h2>
                <div class="dark-line"></div>
                <div class="breif">
                  The Arts and Cultural Heritage Trust has held numerous events in the last eighteen months in the run-up to the establishment of the Museum. Three events are highlighted here, for more information, you can visit our <a href="https://www.facebook.com/ThePartitionMuseumProject/">facebook page</a>.
                </div>
              </div>
              <div class="single-event">
                <div class="dark-line"></div>
                <h2>Young Poets on the Partition</h2>
                <div class="dark-line"></div>
                <div class="breif">
                  On 20th August, 2016 we held an event where over 25 young poets presented their works on the Partition.
                  <div class="youtube-video">
                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="youtube-video">
                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="youtube-video">
                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="youtube-video">
                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="youtube-video">
                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/qpgTC9MDx1o" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
              </div>
              <div class="single-event">
                <div class="dark-line"></div>
                <h2>Rising from the Dust: Hidden Tales from India’s Refugee Camps in 1947</h2>
                <div class="dark-line"></div>
                <div class="breif">
                  In April 2016, we hosted an exhibition for 10 days. The exhibition focused entirely on only one aspect of the Partition: the refugee camps. We showcased art, photographs, letters, personal memorabilia, oral histories, documentaries – all dating back to 1947. These were very real testaments to the challenges faced by the displaced population, and by the newly appointed governments of India and Pakistan. The gallery contained more than 100 exhibits, from private and government archives as well as universities.</br>
                  We also had a lecture series with speakers from UK, France and India, on curating for a Partition Museum.
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                </div>
              </div>
              <div class="single-event">
                <div class="dark-line"></div>
                <h2>India Art Fair</h2>
                <div class="dark-line"></div>
                <div class="breif">
                  In February 2016, we held an event at the India Art Fair where we showcased some objects from the Partition period, and recorded oral histories.
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                  <div class="full-width-image">
                    <img src="public/img/landing-title.png" alt="photos from event" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="about-us">
      <div class="about-us">
        <div class="content">
          <h1>About The Partition Museum- A Museum of Memories to Commemorate the Partition of India</h1>
          <p>We at The Arts and Cultural Heritage Trust, have been recently appointed by the Punjab Government to set up a Partition Museum at the prestigious Town Hall in Amritsar, in time to commemorate the 70th year of the Partition of India.</p>
          <p>This will be primarily a people’s museum as the event will be memorialised through the experiences of the people. The Partition of India led to the migration of over 14 million to a new homeland. Not only did they lose their homes, they also lost lives and loved ones along the way. Their journey sometimes took months and the destination was ridden with hunger and despair. Many were forced to live in refugee camps, for a long time.</p>
          <p>Importantly, this will be a museum of reconciliation and healing but the Partition Museum will also showcase the grit and spirit of the Partition Survivors. Despite losing everything they were able to rise from the dust. Many of them participated wholeheartedly and idealistically in the effort of nation building. They kept their own pain and trauma veiled as they looked after their families and comforted others. This is a poignant yet powerful story, which has never been told before within an experiential museum, anywhere in the world.</p>
          <p>In fact, this will be the first Museum on the Partition of India. This will not only be a place of remembrance it will also become a resource centre for the study and understanding of the Partition. We hope to gather collections here—be it art, documents, oral histories, films, poetry, etc from everywhere so that Partition Survivors, their families, scholars and interested individuals will come here to gain comprehensive knowledge about this very traumatic event.</p>
          <h1>About The Arts and Cultural Heritage Trust</h1>
          <img src="public/img/TAACHT-logo.jpg">
          <p>The main focus of The Arts and Cultural Heritage Trust is to set up a Partition Museum. The Partition Museum Project was initiated by The Arts and Cultural Heritage Trust (TAACHT), in early 2015, to work towards the establishment of a world class, physical museum, dedicated to the memory of the Partition in 1947 — its victims, its survivors and its lasting legacy. Soon it will be seventy years since, but there is no memorial, no designated space, no commemoration of any kind that documents how that migration led to birth of the two nations. The Partition Museum Project initiated by The Arts and Cultural Heritage Trust will create that space.</p>
          <p>Watch a video of the Chair of The Arts and Cultural Heritage Trust (TAACHT), Kishwar Desai, talk about how The Partition Museum project was started and the journey over the last two years:</p>
          <h1>Our Partners:</h1>
          <p>TAACHT is proud to have support from leading organizations around the world:</p>
          <div class="people-involved">
            <h1>People Involved:</h1>
            <ul>
              <li class="title">Chair</li>
              <li>Kishwar Desai</li>
              <li class="title">Trustees</li>
              <li>Ritu Kumar</li>
              <li>Anjolie Ela Menon</li>
              <li>Soni Razdan</li>
              <li>Bela Sethi</li>
              <li>Dipali Khanna</li>
              <li>Bindu Manchanda</li>
              <li>Mallika Ahluwalia</li>
              <li>Vikramjit Sahney</li>
              <li>Sunaina Anand</li>
              <li>Pinky Anand</li>
              <li class="title">Oral Histories and Media</li>
              <li>Poonam Bahl</li>
              <li class="title">Researchers/Volunteers</li>
              <li>Saudiptendo Ray</li>
              <li>Swati Mishra</li>
              <li>Ganeev Kaur Dhillon</li>
              <li>Swati Sharma</li>
              <li class="title">Project Assistants</li>
              <li>Diksha Sharma</li>
              <li>Chief Patron</li>
              <li>Kuldip Nayar</li>
              <li class="title">Board of Patrons</li>
              <li>Meghnad Desai</li>
              <li>Padam Rosha</li>
              <li>Salima Hashmi</li>
              <li>Jugnu Mohsin</li>
              <li>Ashis Nandy</li>
              <li>Shyam Benegal</li>
              <li>Prasoon Joshi</li>
              <li>Chiranjeev Singh</li>
              <li>Ameena Sayyid</li>
              <li>Gurinder Chadha</li>
              <li>Satish Gujral</li>
              <li class="title">Advisory Board Members</li>
              <li>Abha Sawhney</li>
              <li>Gitanjali Chaturvedi</li>
              <li>Ali Sethi</li>
              <li>Ratnesh and Sangeeta Mathur</li>
              <li>Geetika Kalha</li>
              <li>Gurpreet Kaur Maini</li>
              <li>Ashish Khokar</li>
              <li>Nilanjan Sarkar</li>
              <li>Davinder Singh</li>
              <li>Parminder Kaur</li>
              <li>Sukrita Paul Kumar</li>
              <li>Rakshanda Jalil</li>
              <li>Poonam Saxena</li>
              <li>Deepa Mehta</li>
              <li>Sumant Batra</li>
              <li class="title">Project Advisory Board(Design/ Curation/ Collaborations)</li>
              <li>Amardeep Behl</li>
              <li>Dronah</li>
              <li>Shikah Jain</li>
              <li>Saurabh Jain</li>
              <li>Praneet Bubber</li>
              <li class="title">Public Relations</li>
              <li>Anshu Khanna</li>
              <li class="title">Strategy Advisor</li>
              <li>Sonal Narain</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="literature">
      <div class="literature">
        <a class="film-clips" href="film-clips.php">Films Clips<div class="light-line"></div></a>
        <a class="poems" href="poems.php">Poems</a>
        <a class="photos" href="photos.php">Photos</a>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="team">
      <div class="team">
        <div class="member">
          <p class="postion">The partition museum</p>
          <p class="name">Anjolie Ela Menon</p>
          <div class="light-line"></div>
        </div>
        <div class="member">
          <p class="postion">The partition museum</p>
          <p class="name">Anjolie Ela Menon</p>
          <div class="light-line"></div>
        </div>
        <div class="member">
          <p class="postion">The partition museum</p>
          <p class="name">Anjolie Ela Menon</p>
          <div class="light-line"></div>
        </div>
        <div class="member">
          <p class="postion">The partition museum</p>
          <p class="name">Anjolie Ela Menon</p>
          <div class="light-line"></div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="shop">
      <div class="shop">Comming Soon</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="donate">
      <div class="join-donate">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#donate-form" aria-controls="donate-form" role="tab" data-toggle="tab">Donate</a></li>
          <li role="presentation"><a href="#registration" aria-controls="registration" role="tab" data-toggle="tab">Partition registration</a></li>
          <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Contact form</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="donate-form">
            <div class="donate-form">
              <form>
                <div class="form-group username">
                  <label for="name">Name</label>
                  <input type="text" name="name" required>
                </div>
                <div class="form-group emailid">
                  <label for="email">Email ID</label>
                  <input type="email" name="email" required>
                </div>
                <div class="form-group phonenumber">
                  <label for="phone">Phone number</label>
                  <input type="tel" pattern="[789][0-9]{9}" name="phone" required>
                </div>
                <div class="form-group amount">
                  <label for="amt">&#8377; Donation Amount</label>
                  <input type="text" name="amt" required >
                </div>
                <div class="form-group pannumber">
                  <label for="pan">PAN Number</label>
                  <input type="text" name="pan" required>
                </div>
                <div class="form-group">
                  <input type="button" name="submit" value="DONATE NOW">
                </div>
              </form>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="registration">
            <div class="registration-form">
              <form>
                <div class="form-group username">
                  <label for="name">Name</label>
                  <input type="text" name="name" required>
                </div>
                <div class="form-group emailid">
                  <label for="email">Email ID</label>
                  <input type="email" name="email" required>
                </div>
                <div class="form-group phonenumber">
                  <label for="phone">Phone number</label>
                  <input type="text" name="phone" required>
                </div>
                <div class="form-group">
                  <input type="button" name="submit" value="REGISTER">
                </div>
              </form>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="contact">
            <div class="contact-form">
              <form>
                <div class="form-group username">
                  <label for="name">Name</label>
                  <input type="text" name="name" required>
                </div>
                <div class="form-group emailid">
                  <label for="email">Email ID</label>
                  <input type="email" name="email" required>
                </div>
                <div class="form-group phonenumber">
                  <label for="phone">Phone number</label>
                  <input type="text" name="phone" required>
                </div>
                <div class="form-group message">
                  <label for="message">Write a message</label>
                  <input type="text" name="message" required>
                </div>
                <div class="form-group">
                  <input type="button" name="submit" value="SUBMIT">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="map-img">
      <p>Once completed, the Museum will have around 10 galleries spanning two floors of the historic Town Hall building.</p>
      <div class="map-image-container">
        <img src="public/img/map.jpg" alt="museum-map" />
      </div>
      <p>Take a virtual tour through the museum <a href="#">here</a></p>
      <p>As parts of the heritage building are still under renovation, the Partition Museum currently covers galleries A, B, C, and D on the ground floor.</p>
    </div>
  </div>
</div>
<div class="fixed-video-icon">
  <span class="glyphicon glyphicon-facetime-video" data-unicode="e059"></span>
</div>
<script>
  // google map
  function initMap() {
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
      center: new google.maps.LatLng(28.619807, 77.216283),
      zoom: 10
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYnVTJh_YJl-gJoc0pTGQYxyfl51-jDiQ&callback=initMap" type="text/javascript"></script>
<?php include('footer.php'); ?>
