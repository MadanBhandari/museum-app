<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Partition Museum Project</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="film-clips-page">
      <header class="header">
        <div class="back-icon">
          <a href="film-clips.php"><i class="fa fa-angle-left"></i></a>
        </div>
        <div class="page-title">
          <h1>Film Clips</h1>
        </div>
      </header>
      <div class="film-clips">
        <div class="single-item">
          <div class="play-icon">
            <i class="fa fa-play"></i>
          </div>
        </div>
        <div class="description">
          Khamosh Pani is a 2003 Pakistani film about a widowed mother, and her young son set in a late 1970s village in Punjab, Pakistan which is coming under radical influence
        </div>
        <div class="movie-team">
          <div class="dark-line"></div>
          <h2>ABOUT THE TEAM</h2>
          <div class="dark-line"></div>
          <table>
            <tbody>
              <tr>
                <td>Name of the movie:</td>
                <td>Khamosh Pani</td>
              </tr>
              <tr>
                <td>Director:</td>
                <td>Sabiha Sumar</td>
              </tr>
              <tr>
                <td>Actors:</td>
                <td>Kiron Kher</br>Aamir Khan</br>Shilpa Shukla</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <?php include('footer.php'); ?>
